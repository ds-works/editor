import { Component, OnInit } from '@angular/core';
import { TextService } from './text-service/text.service';
import { DatamuseService } from './datamuse-service/datamuse.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Simple Text Editor';
  text = '';
  synonims = [];

  constructor(private textService: TextService,
              private datamuseService: DatamuseService) {
  }

  ngOnInit() {
    this.textService.getMockText().then((result) => this.text = result);
  }

  handleWordSelection(word) {
    const subscription = this.datamuseService.getSynonims(word).subscribe((synonims: any[]) => {
      this.synonims = synonims;
      subscription.unsubscribe();
    });
  }
}
