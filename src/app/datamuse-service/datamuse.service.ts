import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DatamuseService {
  constructor(private http: HttpClient) {}

  getSynonims(word) {
    return this.http.get(`https://api.datamuse.com/words?rel_syn=${word}`);
  }
}
