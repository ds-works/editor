export default interface Word {
    name: string;
    settings: {
        b: boolean;
        i: boolean;
        u: boolean;
        c: string;
    }
}