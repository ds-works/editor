import { Component, Input, AfterViewInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { MdePopoverTrigger } from '@material-extended/mde';
import Word from './models/word.model';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.css']
})
export class EditorComponent implements AfterViewInit {
  @ViewChild(MdePopoverTrigger) trigger: MdePopoverTrigger;

  @Input() text = '';
  @Input() synonims = [];

  @Output() wordSelectEvent = new EventEmitter<string>();

  private editor: Element;
  private selection: Selection;
  private range: Range;
  private words: Word[] = [];
  private selectedWord: string;

  private commands = {
    b: 'bold',
    i: 'italic',
    u: 'underline',
    c: 'foreColor',
    tf: 'indent',
    tb: 'outdent'
  };

  public bActive = false;
  public iActive = false;
  public uActive = false;
  public color = '#000';

  ngAfterViewInit() {
    this.editor = document.querySelector('.file');
    this.editor.addEventListener('mouseup', () => {
      this.synonims = [];
      if (!window.getSelection ||
          !window.getSelection().toString().length ||
          window.getSelection().toString().trim().includes(' ')) {
        this.bActive = false;
        this.iActive = false;
        this.uActive = false;
        this.color = '#000';
        this.trigger.closePopover();
        return;
      }
      this.selection = window.getSelection();
      this.selectedWord = this.selection.toString().trim();
      this.range = this.selection.getRangeAt(0);
      if (!this.words.find(item => item.name === this.selectedWord)) {
        this.words.push({
          name: this.selectedWord,
          settings: {
            b: false,
            i: false,
            u: false,
            c: '#000'
          }
        });
        this.trigger.togglePopover();
      } else {
        const selectedWordSettings = this.words.find(item => item.name === this.selectedWord);
        this.bActive = selectedWordSettings.settings.b;
        this.iActive = selectedWordSettings.settings.i;
        this.uActive = selectedWordSettings.settings.u;
        this.color = selectedWordSettings.settings.c;
        this.trigger.togglePopover();
      }
      this.wordSelectEvent.emit(this.selectedWord);
      return;
    });
  }

  handleTextFormat(formatType, param = null) {
    const command = this.commands[formatType];
    document.execCommand(command, false, param);
    if (this.selection && this.selection.toString().length) {
      if (!this.words.find(item => item.name === this.selectedWord)) {
        return;
      }
      const word = this.words.find(item => item.name === this.selectedWord);
      word.settings[formatType] = !word.settings[formatType];
      if (formatType === 'c') {
        word.settings[formatType] = param;
        this.color = param;
      } else {
        this[`${formatType}Active`] = word.settings[formatType];
      }
    }
    return;
  }

  switchToSynonim(synonim) {
    if (this.selection && this.range) {
      if (!this.words.find(item => item.name === this.selectedWord)) {
        return;
      } else {
        const oldWord = this.words.find(item => item.name === this.selectedWord);
        oldWord.name = synonim;
        this.selectedWord = synonim;
      }
      const newWordNode = document.createTextNode(synonim);
      this.range.deleteContents();
      this.range.insertNode(newWordNode);
    }
  }

}
