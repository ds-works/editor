# Editor

### Required:
NodeJS, NPM or Yarn

### How to run:
1. Run command 'npm install' or 'yarn' from project root directory
2. Run command 'npm start' or 'yarn start'
3. Open http://localhost:4200 in browser
4. For production build run command 'npm run build:prod' or 'yarn build:prod'
